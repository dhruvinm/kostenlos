import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:proste_bezier_curve/proste_bezier_curve.dart';
import 'package:responsive_builder/responsive_builder.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin {
  TabController? _tabController;
  final _scrollController = ScrollController();

  @override
  void initState() {
    _tabController = TabController(length: 3, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _tabController!.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Positioned(
                child: SingleChildScrollView(
              controller: _scrollController,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 60),
                child: Column(
                  children: [
                    Stack(
                      children: [
                        ClipPath(
                          clipper: CustomClipPath(),
                          child: Container(
                            height: 400,
                            color: const Color(0XFFEBF4FF),
                          ),
                        ),
                        ResponsiveBuilder(
                          builder: (context, sizingInformation) {
                            if (sizingInformation.isMobile) {
                              return Column(
                                children: [
                                  const Text(
                                    "Deine Job\nwebsite",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(fontSize: 44, fontFamily: "lato"),
                                  ),
                                  SvgPicture.asset(
                                    "assets/images/handshake.svg",
                                  ),
                                ],
                              );
                            } else {
                              return Padding(
                                padding: const EdgeInsets.only(top: 50),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Column(
                                      children: [
                                        const Text(
                                          "Deine Job\nwebsite",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(fontSize: 44, fontFamily: "lato"),
                                        ),
                                        GestureDetector(
                                          onTap: () {
                                            _scrollController.jumpTo(_scrollController.position.extentInside);
                                          },
                                          child: Container(
                                            margin: const EdgeInsets.symmetric(vertical: 22, horizontal: 0),
                                            padding: const EdgeInsets.symmetric(horizontal: 50),
                                            decoration: BoxDecoration(
                                                gradient: const LinearGradient(
                                                  colors: [
                                                    Color(0XFF319795),
                                                    Color(0XFF3182CE),
                                                  ],
                                                ),
                                                borderRadius: BorderRadius.circular(10)),
                                            child: const Center(
                                              child: Padding(
                                                padding: EdgeInsets.symmetric(vertical: 12),
                                                child: Text(
                                                  "Kostenlos Registrieren",
                                                  style: TextStyle(fontFamily: "lato", fontSize: 14, color: Colors.white),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    const SizedBox(
                                      width: 50,
                                    ),
                                    Container(
                                      height: 300,
                                      width: 300,
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.circular(1000),
                                      ),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(1000),
                                        child: SvgPicture.asset(
                                          "assets/images/handshake.svg",
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: width * .15,
                                    )
                                  ],
                                ),
                              );
                            }
                          },
                        ),
                      ],
                    ),
                    Container(
                      height: 35,
                      margin: const EdgeInsets.only(top: 100),
                      width: 400,
                      decoration: BoxDecoration(
                        border: Border.all(color: const Color(0XFFCBD5E0), width: 1),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: TabBar(
                          onTap: (value) {
                            setState(() {});
                          },
                          controller: _tabController,
                          indicator: BoxDecoration(
                            borderRadius: _tabController?.index != 1
                                ? BorderRadius.only(
                                    topLeft: Radius.circular(_tabController?.index == 0 ? 12 : 0),
                                    bottomLeft: Radius.circular(_tabController?.index == 0 ? 12 : 0),
                                    bottomRight: Radius.circular(_tabController?.index == 2 ? 12 : 0),
                                    topRight: Radius.circular(_tabController?.index == 2 ? 12 : 0))
                                : BorderRadius.circular(0),
                            color: const Color(0XFF81E6D9),
                            // border: Border.symmetric(horizontal: BorderSide(width: 1)),
                          ),
                          labelColor: Colors.white,
                          unselectedLabelColor: const Color(0XFF319795),
                          tabs: const [
                            Tab(
                              text: "Arbeitnehmer",
                            ),
                            Tab(
                              text: "Arbeitgeber",
                            ),
                            Tab(
                              text: "Temporärbüro",
                            ),
                          ]),
                    ),
                    Container(
                      height: 1300,
                      // color: Color(0xffE6FFFA),
                      color: Colors.white,
                      child: TabBarView(
                        controller: _tabController,
                        children: [
                          tabWidget(width, 1),
                          tabWidget(width, 2),
                          tabWidget(width, 3),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            )),
            Positioned(
              bottom: 0,
              child: ResponsiveBuilder(
                builder: (context, sizingInformation) {
                  if (sizingInformation.isMobile) {
                    return Container(
                        width: width,
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(blurRadius: 5, spreadRadius: 1, color: Colors.blue.withOpacity(0.2)),
                          ],
                          color: Colors.white,
                          borderRadius: const BorderRadius.vertical(top: Radius.circular(12)),
                        ),
                        child: Center(
                          child: Container(
                            margin: const EdgeInsets.symmetric(vertical: 22, horizontal: 40),
                            decoration: BoxDecoration(
                                gradient: const LinearGradient(
                                  colors: [Color(0XFF319795), Color(0XFF3182CE)],
                                ),
                                borderRadius: BorderRadius.circular(10)),
                            child: const Center(
                              child: Padding(
                                padding: EdgeInsets.symmetric(vertical: 12),
                                child: Text(
                                  "Kostenlos Registrieren",
                                  style: TextStyle(fontFamily: "lato", fontSize: 14, color: Colors.white),
                                ),
                              ),
                            ),
                          ),
                        ));
                  } else {
                    return Container();
                  }
                },
              ),
            ),
            Positioned(
              top: 0,
              child: Container(
                  height: 65,
                  width: width,
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(blurRadius: 5, spreadRadius: 1, color: Colors.blue.withOpacity(0.2)),
                    ],
                    color: Colors.white,
                    borderRadius: const BorderRadius.vertical(bottom: Radius.circular(12)),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: const [
                      Padding(
                        padding: EdgeInsets.only(right: 19),
                        child: Text(
                          "Login",
                          style: TextStyle(fontFamily: "lato", color: Color(0XFF319795)),
                        ),
                      ),
                    ],
                  )),
            ),
          ],
        ),
      ),
    );
  }

  Widget tabWidget(double width, int from) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        ResponsiveBuilder(
          builder: (context, sizingInformation) {
            if (sizingInformation.isMobile) {
              return Container(
                padding: const EdgeInsets.only(bottom: 40),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 30, bottom: 20),
                      child: Text(
                        from == 1
                            ? "Drei einfache Schritte\nzu deinem neuen Job"
                            : from == 2
                                ? "Drei einfache Schritte\nzu deinem neuen Mitarbeiter"
                                : "Drei einfache Schritte zur\nVermittlung neuer Mitarbeiter",
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontSize: 22, fontFamily: "lato"),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 50),
                      child: SvgPicture.asset(
                        "assets/images/girlwithcard.svg",
                        height: 150,
                        width: 150,
                        // fit: BoxFit.fill,
                      ),
                    ),
                    SizedBox(
                      height: 150,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          const Text(
                            "1.",
                            style: TextStyle(fontSize: 150, fontFamily: "lato", color: Color(0xff718096)),
                          ),
                          Container(
                            width: 20,
                          ),
                          const Expanded(
                            child: Text(
                              "Erstellen dein Unternehmensprofil",
                              style: TextStyle(
                                fontSize: 20,
                                color: Color(0xff718096),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              );
            } else {
              return Container(
                padding: const EdgeInsets.only(bottom: 40, left: 50),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 30, bottom: 20),
                      child: Text(
                        from == 1
                            ? "Drei einfache Schritte\nzu deinem neuen Job"
                            : from == 2
                                ? "Drei einfache Schritte\nzu deinem neuen Mitarbeiter"
                                : "Drei einfache Schritte zur\nVermittlung neuer Mitarbeiter",
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontSize: 22, fontFamily: "lato"),
                      ),
                    ),
                    SizedBox(
                      height: 150,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          SizedBox(
                            width: width * .18,
                          ),
                          const Text(
                            "1.",
                            style: TextStyle(fontSize: 150, fontFamily: "lato", color: Color(0xff718096)),
                          ),
                          Container(
                            width: 20,
                          ),
                          const Expanded(
                            child: Text(
                              "Erstellen dein Unternehmensprofil",
                              style: TextStyle(
                                fontSize: 20,
                                color: Color(0xff718096),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 50),
                            child: SvgPicture.asset(
                              "assets/images/girlwithcard.svg",
                              height: 200,
                              width: 200,
                              // fit: BoxFit.fill,
                            ),
                          ),
                          SizedBox(
                            width: width * .2,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              );
            }
          },
        ),
        ResponsiveBuilder(
          builder: (context, sizingInformation) {
            if (sizingInformation.isMobile) {
              return Stack(
                children: [
                  clippath(width),
                  SizedBox(
                    height: 350,
                    width: double.infinity,
                    child: Column(
                      children: [
                        Container(
                          height: 150,
                          padding: const EdgeInsets.only(left: 50),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              const Text(
                                "2.",
                                style: TextStyle(fontSize: 150, fontFamily: "lato", color: Color(0xff718096)),
                              ),
                              Container(
                                width: 20,
                              ),
                              const Expanded(
                                child: Text(
                                  "Erhalte Vermittlungs- angebot von Arbeitgeber",
                                  style: TextStyle(
                                    fontSize: 20,
                                    color: Color(0xff718096),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        Container(
                          height: 15,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 50),
                          child: SvgPicture.asset(
                            from == 1
                                ? "assets/images/tab1_2.svg"
                                : from == 2
                                    ? "assets/images/tab2_2.svg"
                                    : "assets/images/2.svg",
                            height: 150,
                            width: 200,
                            // fit: BoxFit.fill,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              );
            } else {
              return Stack(
                children: [
                  clippath(width),
                  Positioned.fill(
                    child: SizedBox(
                      height: 350,
                      width: double.infinity,
                      child: Column(
                        children: [
                          Container(
                            height: 150,
                            padding: const EdgeInsets.only(left: 50, top: 0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                SizedBox(
                                  width: width * .2,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 50, top: 50),
                                  child: SvgPicture.asset(
                                    from == 1
                                        ? "assets/images/tab1_2.svg"
                                        : from == 2
                                            ? "assets/images/tab2_2.svg"
                                            : "assets/images/2.svg",
                                    height: 150,
                                    width: 150,
                                    fit: BoxFit.fill,
                                  ),
                                ),
                                const SizedBox(
                                  width: 50,
                                ),
                                const Text(
                                  "2.",
                                  style: TextStyle(fontSize: 150, fontFamily: "lato", color: Color(0xff718096)),
                                ),
                                Container(
                                  width: 20,
                                ),
                                const Expanded(
                                  child: Text(
                                    "Erhalte Vermittlungs- angebot von Arbeitgeber",
                                    style: TextStyle(
                                      fontSize: 20,
                                      color: Color(0xff718096),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              );
            }
          },
        ),
        ResponsiveBuilder(
          builder: (context, sizingInformation) {
            if (sizingInformation.isMobile) {
              return Container(
                height: 400,
                width: double.infinity,
                color: Colors.white,
                child: Stack(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(right: 30),
                      height: 300,
                      width: 300,
                      decoration: BoxDecoration(
                        color: const Color(0XFFF7FAFC),
                        borderRadius: BorderRadius.circular(1000),
                      ),
                    ),
                    Column(
                      children: [
                        Container(
                          padding: const EdgeInsets.only(left: 70),
                          height: 150,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              const Text(
                                "3.",
                                style: TextStyle(fontSize: 150, fontFamily: "lato", color: Color(0xff718096)),
                              ),
                              Container(
                                width: 20,
                              ),
                              const Expanded(
                                child: Text(
                                  "Vermittlung nach Provision oder Stundenlohn",
                                  style: TextStyle(
                                    fontSize: 20,
                                    color: Color(0xff718096),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        Container(
                          height: 40,
                        ),
                        SvgPicture.asset(
                          from == 1
                              ? "assets/images/tab1_3.svg"
                              : from == 2
                                  ? "assets/images/tab2_3.svg"
                                  : "assets/images/3.svg",
                          height: 200,
                          width: 200,
                          // fit: BoxFit.fill,
                        ),
                      ],
                    ),
                  ],
                ),
              );
            } else {
              return Container(
                height: 400,
                width: double.infinity,
                color: Colors.white,
                child: Stack(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(right: 30, left: 70),
                      height: 300,
                      width: 300,
                      decoration: BoxDecoration(
                        color: const Color(0XFFF7FAFC),
                        borderRadius: BorderRadius.circular(1000),
                      ),
                    ),
                    Column(
                      children: [
                        Container(
                          padding: const EdgeInsets.only(left: 140),
                          height: 150,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              const Text(
                                "3.",
                                style: TextStyle(fontSize: 150, fontFamily: "lato", color: Color(0xff718096)),
                              ),
                              Container(
                                width: 20,
                              ),
                              const Expanded(
                                child: Text(
                                  "Vermittlung nach Provision oder Stundenlohn",
                                  style: TextStyle(
                                    fontSize: 20,
                                    color: Color(0xff718096),
                                  ),
                                ),
                              ),
                              SvgPicture.asset(
                                from == 1
                                    ? "assets/images/tab1_3.svg"
                                    : from == 2
                                        ? "assets/images/tab2_3.svg"
                                        : "assets/images/3.svg",
                                height: 200,
                                width: 200,
                                fit: BoxFit.fill,
                              ),
                              SizedBox(
                                width: width * .2,
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              );
            }
          },
        ),
      ],
    );
  }
}

class CustomClipPath extends CustomClipper<Path> {
  var radius = 10.0;
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(0, size.height);
    path.quadraticBezierTo(size.width / 4, size.height - 40, size.width / 2, size.height - 20);
    path.quadraticBezierTo(3 / 4 * size.width, size.height, size.width, size.height - 30);
    path.lineTo(size.width, 0);

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

Widget clippath(double width) {
  return Column(
    children: [
      ClipPath(
        clipper: ProsteBezierCurve(
          position: ClipPosition.top,
          list: [
            BezierCurveSection(
              start: Offset(width, 0),
              top: Offset(width / 4 * 3, 60),
              end: Offset(width / 2, 30),
            ),
            BezierCurveSection(
              start: Offset(width / 2, 30),
              top: Offset(width / 4, 0),
              end: const Offset(0, 30),
            ),
          ],
        ),
        child: Container(
          height: 220,
          color: const Color(0XFFE6FFFA),
        ),
      ),
      ClipPath(
        clipper: ProsteBezierCurve(
          position: ClipPosition.bottom,
          list: [
            BezierCurveSection(
              start: const Offset(0, 150),
              top: Offset(width / 4, 100),
              end: Offset(width / 2, 125),
            ),
            BezierCurveSection(
              start: Offset(width / 2, 125),
              top: Offset(width / 4 * 3, 150),
              end: Offset(width, 125),
            ),
          ],
        ),
        child: Container(
          height: 220,
          color: const Color(0XFFE6FFFA),
        ),
      ),
    ],
  );
}
